import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Chat } from './chat.model';
import { PostChat } from './PostChat.model';
import { ChatServiceService } from '../chat-service.service';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {
  constructor(
    private http: HttpClient,
    private chatservice: ChatServiceService
  ) {}
  currentDate = new Date();
  ngOnInit(): void {
    this.getchat();
  }

  public chat: Chat[] = [];
  public chatbody: string = '';
  public postchat: PostChat = {} as PostChat;

  send() {
    if (this.chatbody.length > 0) {
      this.postchat.message = this.chatbody;

      this.createChat();
    } else {
      alert('Cannot send empty message');
    }
  }
  getchat() {
    this.http.get<Chat[]>('api/chat').subscribe((response) => {
      this.chat = response;
      console.log(this.chat);
    });
  }
  createChat(): void {
    if (this.chatbody.length != 0) {
      this.chatservice.createChat(this.postchat).subscribe((data) => {
        alert('Chat Send');
        this.getchat();
        this.ngOnInit();
        this.chatbody = '';
      });
    }
  }
}