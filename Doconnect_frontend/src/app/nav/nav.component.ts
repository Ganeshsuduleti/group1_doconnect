import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SendchatService } from '../sendchat.service';
import { UserStorageService } from '../services/storage/user-storage.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  isUserLoggedIn: boolean = UserStorageService.isUserLoggedIn();
  isAdminLoggedIn: boolean = UserStorageService.isAdminLoggedIn();
  flag2: boolean;
  constructor(
    public router: Router,
    private sendchatservice: SendchatService
  ) {}

  ngOnInit(): void {
    this.sendchatservice.flag2.subscribe((c) => {
      this.flag2 = c;
    });
    this.router.events.subscribe((event) => {
      if (event.constructor.name === 'NavigationEnd') {
        this.isUserLoggedIn = UserStorageService.isUserLoggedIn();
        this.isAdminLoggedIn = UserStorageService.isAdminLoggedIn();
        console.log(this.isUserLoggedIn);
      }
    });
  }
  openchat() {}
  changeflag2() {
    this.sendchatservice.changeflag2();
    console.log(this.flag2);
  }
  logout() {
    UserStorageService.signOut();
    this.router.navigateByUrl('login');
  }
}
