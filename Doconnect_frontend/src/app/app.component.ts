import { Component, OnInit } from '@angular/core';
import { SendchatService } from './sendchat.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  flag2: boolean;
  constructor(private sendchatservice: SendchatService) {}
  ngOnInit(): void {
    this.sendchatservice.flag2.subscribe((c) => {
      this.flag2 = c;
    });
  }
  title = 'doConnect-web';
}
