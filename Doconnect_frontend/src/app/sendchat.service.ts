import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SendchatService {
  flag1: boolean = false;
  flag2: BehaviorSubject<boolean>;
  constructor() {
    this.flag2 = new BehaviorSubject(this.flag1);
  }
  changeflag2() {
    this.flag2.next((this.flag1 = !this.flag1));
  }
}
