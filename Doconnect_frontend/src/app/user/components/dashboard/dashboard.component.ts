import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { QuestionService } from '../../user-services/question/question.service';
import { Chat } from '../../../chat/chat.model';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  allquestions: any;
  validateForm!: FormGroup;
  tagsForm!: FormGroup;
  listOfOption: Array<{ label: string; value: string }> = [];
  listOfTagOptions = [];
  currentPage: any = 1;
  total: any;
  searchMode = 1;
  isSpinning = false;
  public chat: Chat[] = [];
  constructor(
    private http: HttpClient,
    private questionsService: QuestionService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.getchat();
    this.validateForm = this.fb.group({
      title: [null, [Validators.required]],
    });

    this.submitForm();
  }
  getchat() {
    this.http
      .get<Chat[]>('http://localhost:4200/api/chat')
      .subscribe((response) => {
        this.chat = response;
        console.log(this.chat);
      });
  }
  submitForm(): void {
    this.isSpinning = true;
    console.log('submit', this.validateForm.value);
    this.questionsService
      .searchQuestionByTitle(
        this.currentPage - 1,
        this.validateForm.controls['title'].value
      )
      .subscribe((res) => {
        console.log(res);
        this.total = res.data.totalPages * 5;
        this.allquestions = res.data.questionDtoList;
        this.isSpinning = false;
      });
  }

  pageIndexChange(value) {
    console.log(value);
    this.currentPage = value;
    this.submitForm();
  }
}

// getchat(){
//   this.http.get<Chat[]>('/api/questions').subscribe((response) => {
//     this.chat = response;
//     console.log(this.chat);
// }
