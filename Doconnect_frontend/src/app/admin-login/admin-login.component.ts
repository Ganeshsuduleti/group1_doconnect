import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { AdminAuthService } from '../services/adminAuth/admin-auth.service';
import { AuthService } from '../services/auth/auth.service';
import { UserStorageService } from '../services/storage/user-storage.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit {

  validateForm!: FormGroup;
  isSpinning = false;

  submitForm(): void {
    this.isSpinning = true;
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    const data = {
      username:this.validateForm.get(['userName'])!.value,
      password:this.validateForm.get(['password'])!.value
    }
    this.adminAuthService.sigin(data).subscribe(res=>{
     this.isSpinning = false;
     if(res.status = 'OK' && data.username=='narendra@gmail.com' && data.password=='narendra')
     {
      console.log(res.data);
      this.userStorageService.saveUser(res.data);
      this.router.navigateByUrl('admin/dashboard');
     }
     else{
      this.notification
      .error(
        'Sorry',
        `${res.message}`,
        { nzDuration: 500 }
      );
     }
  
       
    })

  }

  constructor(private fb: FormBuilder, 
              private adminAuthService:AdminAuthService,
              private notification: NzNotificationService,
              private userStorageService: UserStorageService,
              private router: Router,) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }




}