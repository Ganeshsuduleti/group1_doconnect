import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Chat } from './chat/chat.model';

@Injectable({
  providedIn: 'root',
})
export class ChatServiceService {
  private chatUrl: string;
  constructor(private http: HttpClient) {
    this.chatUrl = 'http://localhost:8080/api/chat';
  }
  public createChat(chat: Chat) {
    return this.http.post<Chat>(this.chatUrl, chat);
  }
}
