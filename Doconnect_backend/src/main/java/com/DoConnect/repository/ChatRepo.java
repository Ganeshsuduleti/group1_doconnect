package com.DoConnect.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.DoConnect.entities.Chat;

@Repository
public interface ChatRepo extends JpaRepository<Chat,Integer>{

}
