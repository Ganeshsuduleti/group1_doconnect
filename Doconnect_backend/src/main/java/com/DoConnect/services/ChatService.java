package com.DoConnect.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.DoConnect.entities.Chat;
import com.DoConnect.repository.ChatRepo;

@Service
public class ChatService {

 @Autowired
 ChatRepo chatrepo;
 public List<Chat> getallchat(){
	 return chatrepo.findAll();
 }
 public void saveChat(Chat chat) {
	 chatrepo.save(chat);
 }
}
