package com.DoConnect.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.DoConnect.entities.Chat;
import com.DoConnect.services.ChatService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="http://localhost:4200")
public class ChatController {
	@Autowired
	ChatService chatservice;
	@GetMapping("/chat")
	public List<Chat> getallchat(){
		return chatservice.getallchat();
	}
	@PostMapping("/chat")
	public void insertchat(@RequestBody Chat chat) {
		chatservice.saveChat(chat);
	}

}
